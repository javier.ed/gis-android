package ml.bululu.android.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.widget.doOnTextChanged
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import ml.bululu.android.R

object FormUtils {
    fun setRequiredFields(context: Context, fields: Array<Pair<Any, TextInputLayout>>) {
        fields.forEach {
            setRequiredField(context, it.first, it.second)
        }
    }

    fun setRequiredField(context: Context, editText: Any, textInputLayout: TextInputLayout) {
        when (editText) {
            is TextInputEditText -> {
                editText.doOnTextChanged { _, _, _, _ ->
                    validateRequiredField(context, editText, textInputLayout)
                }
            }
            is EditText -> {
                editText.doOnTextChanged { _, _, _, _ ->
                    validateRequiredField(context, editText, textInputLayout)
                }
            }
        }

    }

    fun validateRequiredFields(context: Context, fields: Array<Pair<Any, TextInputLayout>>): Boolean {
        var isValid = true
        fields.forEach {
            when (it.first) {
                is TextInputEditText -> {
                    if (!validateRequiredField(context, it.first as TextInputEditText, it.second)) {
                        isValid = false
                    }
                }
                is EditText -> {
                    if (!validateRequiredField(context, it.first as EditText, it.second)) {
                        isValid = false
                    }
                }
            }
        }
        return isValid
    }
    
    fun validateRequiredField(context: Context, editText: TextInputEditText, textInputLayout: TextInputLayout): Boolean {
        val errorMessage = errorMessageForRequiredString(context, editText.text.toString())
        return if (errorMessage != null) {
            textInputLayout.error = errorMessage
            false
        } else {
            textInputLayout.isErrorEnabled = false
            true
        }
    }

    private fun validateRequiredField(context: Context, editText: EditText, textInputLayout: TextInputLayout): Boolean {
        val errorMessage = errorMessageForRequiredString(context, editText.text.toString())
        return if (errorMessage != null) {
            textInputLayout.error = errorMessage
            false
        } else {
            textInputLayout.isErrorEnabled = false
            true
        }
    }

    fun errorMessageForRequiredString(context: Context, value: String?): String? {
        return if (value.isNullOrBlank()) {
            context.getString(R.string.cant_be_blank)
        } else {
            null
        }
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        val view = if (activity.currentFocus != null) { activity.currentFocus
        } else { View(activity) }
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }
}