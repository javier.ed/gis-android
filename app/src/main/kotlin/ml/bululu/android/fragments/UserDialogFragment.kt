package ml.bululu.android.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.apollographql.apollo.exception.ApolloException
import kotlinx.android.synthetic.main.dialog_fragment_user.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ml.bululu.android.R
import ml.bululu.android.activities.SettingsActivity
import ml.bululu.android.utils.AvatarsHandler
import ml.bululu.android.utils.GraphQL
import ml.bululu.android.utils.Session

class UserDialogFragment : DialogFragment() {
    private lateinit var dialogView: View
    private val session by lazy { Session(context as Context) }

    companion object {
        @JvmStatic
        fun newInstance() = UserDialogFragment()
    }

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        dialogView = requireActivity().layoutInflater.inflate(R.layout.dialog_fragment_user, null)
        AvatarsHandler(session.username.toString(), dialogView.avatarImageView)
        dialogView.usernameTextView.text = session.username
        loadGoToSettingsButton()
        loadLogoutButton()
        return AlertDialog.Builder(context).setView(dialogView).create()
    }
    
    private fun loadGoToSettingsButton() {
        dialogView.goToSettingsButton.setOnClickListener {
            startActivity(Intent(context, SettingsActivity::class.java))
        }
    }

    private fun loadLogoutButton() {
        dialogView.logoutButton.setOnClickListener {
            val logoutDialog = AlertDialog.Builder(context)
            logoutDialog.setMessage(R.string.logout_confirmation)
            logoutDialog.setPositiveButton(android.R.string.ok) { _, _ ->
                Toast.makeText(context, R.string.logging_out, Toast.LENGTH_LONG).show()
                CoroutineScope(Dispatchers.IO).launch {
                    val graphQL = GraphQL(context as Context)
                    val logoutMutation = graphQL.mutations.logoutMutationAsync()
                    try {
                        val logout = logoutMutation.await().data?.logout
                        withContext(Dispatchers.Main) {
                            Toast.makeText(context, logout?.message, Toast.LENGTH_LONG).show()
                            if (logout?.success == true) {
                                session.clear()
                                graphQL.apolloClientWithCache.clearNormalizedCache()
                                activity?.finish()
                            }
                        }
                    } catch (e: ApolloException) {
                        e.printStackTrace()
                        withContext(Dispatchers.Main) {
                            Toast.makeText(context, R.string.failed_to_execute_request, Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }

            logoutDialog.setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.cancel()
            }

            logoutDialog.create().show()
        }
    }
}