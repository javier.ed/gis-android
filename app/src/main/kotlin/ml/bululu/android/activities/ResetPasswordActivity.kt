package ml.bululu.android.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.apollographql.apollo.exception.ApolloException
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_reset_password.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ml.bululu.android.R
import ml.bululu.android.utils.FormUtils
import ml.bululu.android.utils.GraphQL
import ml.bululu.android.utils.Session

class ResetPasswordActivity : AppCompatActivity() {
    private var login = ""
    private val requiredFields by lazy {
        arrayOf(
            Pair<Any, TextInputLayout>(confirmationCodeEditText, confirmationCodeTextInputLayout),
            Pair<Any, TextInputLayout>(newPasswordEditText, newPasswordTextInputLayout)
        )
    }

    companion object {
        const val LOGIN = "LOGIN"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val extras = intent.extras ?: return
        login = extras.getString(LOGIN).toString()

        FormUtils.setRequiredFields(this, requiredFields)

        resetPasswordButton.setOnClickListener {
            attemptToResetPassword()
        }
    }

    override fun onStart() {
        super.onStart()
        Session(this).requireNoAuthentication()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun attemptToResetPassword() {
        FormUtils.hideKeyboard(this)
        container.showProgressBar(true)

        if (!FormUtils.validateRequiredFields(this, requiredFields)) {
            container.showContent(true)
            return
        }

        CoroutineScope(Dispatchers.IO).launch {
            val resetPasswordMutation =
                GraphQL(this@ResetPasswordActivity).mutations.resetPasswordMutationAsync(
                    login,
                    confirmationCodeEditText.text.toString(),
                    newPasswordEditText.text.toString()
                )

            try {
                val resetPassword = resetPasswordMutation.await().data?.resetPassword
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        this@ResetPasswordActivity,
                        resetPassword?.message,
                        Toast.LENGTH_LONG
                    ).show()
                }
                if (resetPassword?.success == true) {
                    startActivity(Intent(this@ResetPasswordActivity, LoginActivity::class.java))
                    finish()
                } else {
                    resetPassword?.errors?.forEach {
                        if (it.path?.get(0) == "confirmationCode") {
                            confirmationCodeTextInputLayout.error = it.message
                        } else if (it.path?.get(1) == "password") {
                            newPasswordTextInputLayout.error = it.message
                        }
                    }
                }
            } catch (e: ApolloException) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        this@ResetPasswordActivity,
                        R.string.failed_to_execute_request,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            withContext(Dispatchers.Main) {
                container.showContent(true)
            }
        }
    }
}
