package ml.bululu.android.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.apollographql.apollo.exception.ApolloException
import kotlinx.android.synthetic.main.dialog_fragment_layer_info.view.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import ml.bululu.android.R
import ml.bululu.android.utils.GraphQL

class LayerInfoDialogFragment : DialogFragment() {
    private lateinit var dialogView: View
    private lateinit var layerJob: Job
    private var layerId: String? = null

    companion object {
        const val ID = "ID"

        @JvmStatic
        fun newInstance(id: String) = LayerInfoDialogFragment().apply {
            arguments = Bundle().apply {
                putString(ID, id)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { layerId = it.getString(ID) }
    }

    @ExperimentalCoroutinesApi
    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        dialogView = requireActivity().layoutInflater.inflate(R.layout.dialog_fragment_layer_info, null)
        loadLayer()
        return AlertDialog.Builder(context).setView(dialogView).create()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::layerJob.isInitialized) { layerJob.cancel() }
    }

    @ExperimentalCoroutinesApi
    private fun loadLayer() {
        layerJob = CoroutineScope(Dispatchers.IO).launch {
            val layerQuery = GraphQL(context as Context).queries.layerQueryFlow(layerId.toString())
            try {
                layerQuery.collect {
                    val layer = it.data?.layer

                    if (layer != null) {
                        dialogView.nameTextView.text = layer.name
                        dialogView.descriptionTextView.text = layer.description
                    } else {
                        withContext(Dispatchers.Main) {
                            Toast.makeText(
                                activity,
                                R.string.resource_not_found,
                                Toast.LENGTH_LONG
                            ).show()
                            dismiss()
                        }
                    }
                }
            } catch (e: ApolloException) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        activity,
                        R.string.failed_to_execute_request,
                        Toast.LENGTH_LONG
                    ).show()
                    dismiss()
                }
            }

            withContext(Dispatchers.Main) {
                dialogView.container.showContent(true)
            }
        }
    }
}