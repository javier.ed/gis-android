package ml.bululu.android.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.apollographql.apollo.exception.ApolloException
import kotlinx.android.synthetic.main.activity_email_settings.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import ml.bululu.android.R
import ml.bululu.android.utils.FormUtils
import ml.bululu.android.utils.GraphQL
import ml.bululu.android.utils.Session

class EmailSettingsActivity : AppCompatActivity() {
    private lateinit var currentUserJob: Job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_email_settings)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    @ExperimentalCoroutinesApi
    override fun onStart() {
        super.onStart()
        Session(this).requireAuthentication()
        loadEmail()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    @ExperimentalCoroutinesApi
    private fun loadEmail() {
        currentUserJob = CoroutineScope(Dispatchers.IO).launch {
            val currentUserQuery =
                GraphQL(this@EmailSettingsActivity).queries.currentUserQueryFlow()

            try {
                currentUserQuery.collect {
                    val currentUser = it.data?.currentUser
                    withContext(Dispatchers.Main) {
                        emailTextView.text = currentUser?.email

                        if (currentUser?.emailVerified == true) {
                            unverifiedTextView.visibility = View.GONE
                            verifyEmailLayout.visibility = View.GONE
                        } else {
                            unverifiedTextView.visibility = View.VISIBLE
                            verifyEmailLayout.visibility = View.VISIBLE
                            loadVerifyEmailButton()
                        }
                    }
                }
            } catch (e: ApolloException) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        this@EmailSettingsActivity,
                        R.string.failed_to_execute_request,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            withContext(Dispatchers.Main) {
                container.showContent(true)
            }
        }
    }

    private fun loadVerifyEmailButton() {
        if (verifyEmailButton.hasOnClickListeners()) {
            return
        }

        FormUtils.setRequiredField(
            this@EmailSettingsActivity,
            confirmationCodeEditText,
            confirmationCodeTextInputLayout
        )

        verifyEmailButton.setOnClickListener {
            FormUtils.hideKeyboard(this)
            container.showProgressBar(true)

            if (!FormUtils.validateRequiredField(
                    this@EmailSettingsActivity,
                    confirmationCodeEditText,
                    confirmationCodeTextInputLayout
                )
            ) {
                container.showContent(true)
                return@setOnClickListener
            }

            CoroutineScope(Dispatchers.IO).launch {
                val verifyEmailMutation =
                    GraphQL(this@EmailSettingsActivity).mutations.verifyEmailMutationAsync(
                        confirmationCodeEditText.text.toString()
                    )

                try {
                    val verifyEmail = verifyEmailMutation.await().data?.verifyEmail
                    withContext(Dispatchers.Main) {
                        Toast.makeText(
                            this@EmailSettingsActivity,
                            verifyEmail?.message,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    if (verifyEmail?.success == true) {
                        finish()
                    }
                } catch (e: ApolloException) {
                    e.printStackTrace()
                    withContext(Dispatchers.Main) {
                        Toast.makeText(
                            this@EmailSettingsActivity,
                            R.string.failed_to_execute_request,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
                withContext(Dispatchers.Main) {
                    container.showContent(true)
                }
            }
        }
    }
}
