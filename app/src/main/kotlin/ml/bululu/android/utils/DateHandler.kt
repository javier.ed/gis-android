package ml.bululu.android.utils

import android.text.format.DateUtils
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateHandler {
    const val DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    const val DATE_FORMAT = "yyyy-MM-dd"

    fun stringify(source: java.util.Date?, pattern: String = DATETIME_FORMAT) : String? {
        return if (source != null) {
            val simpleDateFormat = SimpleDateFormat(pattern, Locale.getDefault())
            simpleDateFormat.timeZone = TimeZone.getTimeZone("GMT")
            simpleDateFormat.format(source)
        }
        else { null }
    }

    fun parse(source: String, pattern: String = DATETIME_FORMAT, timeZone: TimeZone = TimeZone.getTimeZone("GMT")) : java.util.Date? {
        return try {
            val simpleDateFormat = SimpleDateFormat(pattern, Locale.getDefault())
            simpleDateFormat.timeZone = timeZone
            simpleDateFormat.parse(source)
        }
        catch (e: ParseException) {
            e.printStackTrace()
            null
        }
    }

    fun timeAgo(time: Long) : CharSequence {
        val now = System.currentTimeMillis()
        return DateUtils.getRelativeTimeSpanString(time, now, 0, DateUtils.FORMAT_ABBREV_RELATIVE)
    }
}
