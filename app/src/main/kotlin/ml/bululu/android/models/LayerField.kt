package ml.bululu.android.models

data class LayerField(
    var type: String = "",
    var name: String = "",
    var showAdvancedOptions: Boolean = false,
    var description: String = "",
    var required: Boolean = false,
    var unique: Boolean = false,
    var editable: Boolean = true,
    var options: MutableList<String> = mutableListOf(),
    var errorType: String? = null,
    var errorName: String? = null,
    var errorOptions: String? = null
)
