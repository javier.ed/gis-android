package ml.bululu.android.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputLayout
import com.robertlevonyan.views.expandable.Expandable
import kotlinx.android.synthetic.main.item_layer_field.view.*
import ml.bululu.android.R
import ml.bululu.android.models.LayerField
import ml.bululu.android.utils.FormUtils

class LayerFieldsAdapter(private val layerFields: MutableList<LayerField>) : RecyclerView.Adapter<LayerFieldsAdapter.LayerFieldsViewHolder>() {
    class LayerFieldsViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    private val typeValues = arrayOf(
        "string",
        "text",
        "integer",
        "float",
        "boolean",
        "date",
        "time",
        "datetime",
        "choice"
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LayerFieldsViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.item_layer_field, parent, false)
        return LayerFieldsViewHolder(v)
    }

    override fun onBindViewHolder(holder: LayerFieldsViewHolder, position: Int) {
        val layerField = layerFields[position]

        val typesAdapter = ArrayAdapter.createFromResource(
            holder.view.context,
            R.array.layer_field_types,
            android.R.layout.simple_list_item_1
        )
        holder.view.typesAutoCompleteTextView.setAdapter(typesAdapter)

        val typeValueIndex = typeValues.indexOf(layerField.type)
        if (typeValueIndex >= 0) {
            holder.view.typesAutoCompleteTextView.setText(typesAdapter.getItem(typeValueIndex))
        }
        holder.view.nameEditText.setText(layerField.name)

        holder.view.advancedOptionsExpandable.animateExpand = false
        if (layerField.showAdvancedOptions) {
            holder.view.advancedOptionsExpandable.expand()
        } else {
            holder.view.advancedOptionsExpandable.collapse()
        }
        holder.view.advancedOptionsExpandable.animateExpand = true

        holder.view.descriptionEditText.setText(layerField.description)
        holder.view.requiredSwitch.isChecked = layerField.required
        holder.view.uniqueSwitch.isChecked = layerField.unique
        holder.view.editableSwitch.isChecked = layerField.editable

        val requiredFields = arrayOf(
            Pair<Any, TextInputLayout>(
                holder.view.typesAutoCompleteTextView,
                holder.view.typesTextInputLayout
            ),
            Pair<Any, TextInputLayout>(
                holder.view.nameEditText,
                holder.view.nameTextInputLayout
            )
        )

        FormUtils.setRequiredFields(holder.view.context, requiredFields)

        if (layerField.errorType != null) {
            holder.view.typesTextInputLayout.error = layerField.errorType
        } else {
            holder.view.typesTextInputLayout.isErrorEnabled = false
        }

        if (layerField.errorName != null) {
            holder.view.nameTextInputLayout.error = layerField.errorName
        } else {
            holder.view.nameTextInputLayout.isErrorEnabled = false
        }

        holder.view.typesAutoCompleteTextView.doOnTextChanged { text, _, _, _ ->
            val value = typeValues.getOrNull(typesAdapter.getPosition(text.toString()))
            layerField.type = value ?: ""
            holder.view.optionsTextInputLayout.visibility = if (layerField.type == "choice") {
                View.VISIBLE
            } else {
                holder.view.optionsEditText.setText("")
                View.GONE
            }
        }

        holder.view.typesAutoCompleteTextView.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus && layerField.type.isBlank()) {
                holder.view.typesAutoCompleteTextView.setText("")
            }
        }

        holder.view.nameEditText.doOnTextChanged { text, _, _, _ ->
            layerField.name = text.toString()
        }

        holder.view.descriptionEditText.doOnTextChanged { text, _, _, _ ->
            layerField.description = text.toString()
        }

        holder.view.optionsEditText.doOnTextChanged { text, _, _, _ ->
            layerField.options =
                text?.split("\n")?.filter { it.isNotBlank() } as MutableList<String>
        }

        holder.view.advancedOptionsExpandable.expandingListener = object : Expandable.ExpandingListener {
            override fun onCollapsed() {
                layerField.showAdvancedOptions = false
            }

            override fun onExpanded() {
                layerField.showAdvancedOptions = true
            }
        }

        holder.view.requiredSwitch.setOnCheckedChangeListener { compoundButton, _ ->
            layerField.required = compoundButton.isChecked
        }

        holder.view.uniqueSwitch.setOnCheckedChangeListener { compoundButton, _ ->
            layerField.unique = compoundButton.isChecked
        }

        holder.view.editableSwitch.setOnCheckedChangeListener { compoundButton, _ ->
            layerField.editable = compoundButton.isChecked
        }

        holder.view.removeButton.setOnClickListener {
            layerFields.removeAt(position)
            notifyDataSetChanged()
        }

        holder.setIsRecyclable(false)
    }

    override fun getItemCount() = layerFields.size
}
