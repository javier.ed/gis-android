package ml.bululu.android.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.apollographql.apollo.exception.ApolloException
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ml.bululu.android.R
import ml.bululu.android.utils.FormUtils
import ml.bululu.android.utils.GraphQL
import ml.bululu.android.utils.Session

class RegisterActivity : AppCompatActivity() {
    private val requiredFields  by lazy {
        arrayOf(
            Pair<Any, TextInputLayout>(usernameEditText, usernameTextInputLayout),
            Pair<Any, TextInputLayout>(emailEditText, emailTextInputLayout),
            Pair<Any, TextInputLayout>(passwordEditText, passwordTextInputLayout)
        )
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        goToLoginButton.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

        FormUtils.setRequiredFields(this, requiredFields)

        registerButton.setOnClickListener { attemptToRegister() }
    }

    override fun onStart() {
        super.onStart()
        Session(this).requireNoAuthentication()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun attemptToRegister() {
        FormUtils.hideKeyboard(this)
        container.showProgressBar(true)

        if (!FormUtils.validateRequiredFields(this, requiredFields)) {
            container.showContent(true)
            return
        }

        CoroutineScope(Dispatchers.IO).launch {
            val registerMutation = GraphQL(this@RegisterActivity).mutations.registerMutationAsync(
                usernameEditText.text.toString(),
                emailEditText.text.toString(),
                passwordEditText.text.toString()
            )

            try {
                val register = registerMutation.await().data?.register
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        this@RegisterActivity,
                        register?.message,
                        Toast.LENGTH_LONG
                    ).show()
                }
                if (register?.success == true) {
                    Session(this@RegisterActivity).update(
                        register.session?.id?.toInt() as Int,
                        register.session.key,
                        register.user?.id?.toInt() as Int,
                        register.user.username
                    )
                    val intent = Intent(Intent(this@RegisterActivity, MainActivity::class.java))
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                    finish()
                } else {
                    withContext(Dispatchers.Main) {
                        register?.errors?.forEach {
                            when (it.path?.get(1)) {
                                "username" -> usernameTextInputLayout.error = it.message
                                "email" -> emailTextInputLayout.error = it.message
                                "password" -> passwordTextInputLayout.error = it.message
                            }
                        }
                    }
                }
            } catch (e: ApolloException) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        this@RegisterActivity,
                        R.string.failed_to_execute_request,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            withContext(Dispatchers.Main) {
                container.showContent(true)
            }
        }
    }
}
