package ml.bululu.android.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_layer.view.*
import ml.bululu.android.R
import ml.bululu.android.activities.MainActivity
import ml.bululu.android.fragments.LayerInfoDialogFragment
import ml.bululu.android.graphql.queries.LayersQuery

class LayersAdapter(private val layers: List<LayersQuery.Node>, private val activity: AppCompatActivity): RecyclerView.Adapter<LayersAdapter.LayersViewHolder>() {
    class LayersViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LayersViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_layer, parent, false)
        return LayersViewHolder(v)
    }

    override fun onBindViewHolder(holder: LayersViewHolder, position: Int) {
        val layer = layers[position]

        holder.view.nameTextView.text = layer.name
        holder.view.descriptionTextView.text = layer.description

        holder.view.layerCardView.setOnClickListener {
            val intent = Intent(activity, MainActivity::class.java)
            intent.putExtra(MainActivity.LAYER_ID, layer.id)
            activity.startActivity(intent)
            activity.finish()
        }

        holder.view.layerInfoButton.setOnClickListener {
            LayerInfoDialogFragment.newInstance(layer.id)
                .show(activity.supportFragmentManager, "layer_${layer.id}")
        }
    }

    override fun getItemCount() = layers.size
}