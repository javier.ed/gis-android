package ml.bululu.android.utils.graphql

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.api.toInput
import com.apollographql.apollo.coroutines.toDeferred
import kotlinx.coroutines.Deferred
import ml.bululu.android.graphql.mutations.*
import ml.bululu.android.graphql.type.LayerFieldInput
import ml.bululu.android.graphql.type.LayerInput
import ml.bululu.android.graphql.type.PasswordInput
import ml.bululu.android.graphql.type.UserInput
import ml.bululu.android.models.LayerField

class Mutations(private val apolloClient: ApolloClient) {
    fun registerMutationAsync(
        username: String,
        email: String,
        password: String
    ): Deferred<Response<RegisterMutation.Data>> {
        val attributes = UserInput(username, email, password)
        val registerMutation = RegisterMutation(attributes)
        return apolloClient.mutate(registerMutation).toDeferred()
    }

    fun loginMutationAsync(
        login: String,
        password: String
    ): Deferred<Response<LoginMutation.Data>> {
        val loginMutation = LoginMutation(login, password)
        return apolloClient.mutate(loginMutation).toDeferred()
    }

    fun logoutMutationAsync(): Deferred<Response<LogoutMutation.Data>> {
        val logoutMutation = LogoutMutation()
        return apolloClient.mutate(logoutMutation).toDeferred()
    }

    fun requestPasswordResetMutationAsync(login: String): Deferred<Response<RequestPasswordResetMutation.Data>> {
        val requestPasswordResetMutation = RequestPasswordResetMutation(login)
        return apolloClient.mutate(requestPasswordResetMutation).toDeferred()
    }

    fun resetPasswordMutationAsync(
        login: String,
        confirmationCode: String,
        password: String
    ): Deferred<Response<ResetPasswordMutation.Data>> {
        val passwordInput = PasswordInput(password)
        val resetPasswordMutation = ResetPasswordMutation(login, confirmationCode, passwordInput)
        return apolloClient.mutate(resetPasswordMutation).toDeferred()
    }

    fun verifyEmailMutationAsync(confirmationCode: String): Deferred<Response<VerifyEmailMutation.Data>> {
        val verifyEmailMutation = VerifyEmailMutation(confirmationCode)
        return apolloClient.mutate(verifyEmailMutation).toDeferred()
    }

    fun createLayerMutationAsync(
        name: String,
        description: String,
        fields: List<LayerField>
    ): Deferred<Response<CreateLayerMutation.Data>> {
        val attributes = LayerInput(
            name,
            description,
            fields.map { field ->
                LayerFieldInput(
                    field.type,
                    field.name,
                    field.description.toInput(),
                    field.required.toInput(),
                    field.unique.toInput(),
                    field.editable.toInput(),
                    field.options.toInput()
                )
            }.toInput()
        )
        val createLayerMutation = CreateLayerMutation(attributes)
        return apolloClient.mutate(createLayerMutation).toDeferred()
    }
}