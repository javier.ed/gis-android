package ml.bululu.android.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.exception.ApolloException
import kotlinx.android.synthetic.main.activity_layers.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import ml.bululu.android.R
import ml.bululu.android.adapters.LayersAdapter
import ml.bululu.android.graphql.queries.LayersQuery
import ml.bululu.android.utils.FormUtils
import ml.bululu.android.utils.GraphQL

class LayersActivity : AppCompatActivity() {
    private var searchMenuItem: MenuItem? = null
    private lateinit var layersJob: Job
    private val layers = mutableListOf<LayersQuery.Node>()
    private var hasNextPage = false
    private var endCursor: String? = null
    private var currentQuery: String? = null
    private var isRefreshing = false

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_layers)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        layersRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = LayersAdapter(layers, this@LayersActivity)
        }

        layersSwipeRefreshLayout.setOnRefreshListener {
            loadLayers(first = 14)
        }

        searchFAB.setOnClickListener {
            showSearchView()
        }

        loadMoreLayers()
    }

    @ExperimentalCoroutinesApi
    override fun onStart() {
        super.onStart()
        layersSwipeRefreshLayout.isRefreshing = true
        loadLayers(first = 14)
    }

    override fun onStop() {
        super.onStop()
        hasNextPage = false
        endCursor = null
        currentQuery = null
        isRefreshing = false
    }


    override fun onDestroy() {
        super.onDestroy()
        cancelJob()
    }

    private fun cancelJob() {
        if (::layersJob.isInitialized) {
            layersJob.cancel()
        }
    }

    @ExperimentalCoroutinesApi
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.layers, menu)
        searchMenuItem = menu?.findItem(R.id.searchMenuItem)

        (searchMenuItem?.actionView as SearchView).apply {
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextChange(query: String): Boolean {
                    setCurrentQuery(query)
                    Handler().postDelayed({
                        if (currentQuery == query) {
                            searchLayers()
                        }
                    }, 500)
                    return false
                }

                override fun onQueryTextSubmit(query: String): Boolean {
                    setCurrentQuery(query)
                    searchLayers()
                    return false
                }

                private fun setCurrentQuery(query: String) {
                    currentQuery = query.trim()
                }

                private fun searchLayers() {
                    layersSwipeRefreshLayout.isRefreshing = true
                    layers.clear()
                    loadLayers(first = 14)
                }
            })

            setOnCloseListener {
                hideSearchView()
                true
            }
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.searchMenuItem -> {
                showSearchView()
            }
            R.id.goToNewLayerMenuItem -> {
                startActivity(Intent(this, NewLayerActivity::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun showSearchView() {
        searchFAB.isVisible = false
        searchMenuItem?.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
        (searchMenuItem?.actionView as SearchView).isIconified = false
    }

    @ExperimentalCoroutinesApi
    private fun hideSearchView() {
        FormUtils.hideKeyboard(this)
        searchMenuItem?.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER)
        searchFAB.isVisible = true
        currentQuery = null
        loadLayers(first = 14)
    }

    @ExperimentalCoroutinesApi
    private fun loadLayers(after: String? = null, first: Int? = null) {
        cancelJob()
        if (!isRefreshing) {
            hasNextPage = false
        }
        layersJob = CoroutineScope(Dispatchers.IO).launch {
            val layersQuery =
                GraphQL(this@LayersActivity).queries.layersQueryFlow(currentQuery, after, first)

            try {
                layersQuery.collect {
                    withContext(Dispatchers.Main) {
                        it.data?.layers?.nodes?.filter { n -> layers.find { l -> l.id == n?.id } == null }
                            ?.forEach { node ->
                                val index = layers.indexOfFirst { l -> l.id.toInt() < (node?.id?.toInt() as Int) }
                                if (index == -1) {
                                    layers.add(node as LayersQuery.Node)
                                } else {
                                    layers.add(index, node as LayersQuery.Node)
                                }
                            }
                        layersRecyclerView.adapter?.notifyDataSetChanged()
                    }

                    it.data?.layers?.pageInfo?.let { pageInfo ->
                        if (!isRefreshing) {
                            hasNextPage = pageInfo.hasNextPage
                            endCursor = pageInfo.endCursor
                        }
                    }
                }
            } catch (e: ApolloException) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        this@LayersActivity,
                        R.string.failed_to_execute_request,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            withContext(Dispatchers.Main) {
                layersSwipeRefreshLayout.isRefreshing = false
                infiniteScrollProgressBar.isVisible = false
                isRefreshing = false
            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun loadMoreLayers() {
        layersRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (hasNextPage && !isRefreshing && layersRecyclerView.canScrollVertically(1)) {
                    infiniteScrollProgressBar.isVisible = true
                    loadLayers(endCursor, first = 14)
                }
            }
        })
    }
}
