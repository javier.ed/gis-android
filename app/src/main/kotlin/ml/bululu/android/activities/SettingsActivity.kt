package ml.bululu.android.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.apollographql.apollo.exception.ApolloException
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import ml.bululu.android.R
import ml.bululu.android.utils.GraphQL
import ml.bululu.android.utils.Session

class SettingsActivity : AppCompatActivity() {
    private lateinit var currentUserJob: Job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        goToEmailSettingsCardView.setOnClickListener {
            startActivity(Intent(this, EmailSettingsActivity::class.java))
        }
    }

    @ExperimentalCoroutinesApi
    override fun onStart() {
        super.onStart()
        Session(this).requireAuthentication()
        loadCurrentUser()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::currentUserJob.isInitialized) {
            currentUserJob.cancel()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    @ExperimentalCoroutinesApi
    private fun loadCurrentUser() {
        currentUserJob = CoroutineScope(Dispatchers.IO).launch {
            val currentUserQuery = GraphQL(this@SettingsActivity).queries.currentUserQueryFlow()

            try {
                currentUserQuery.collect {
                    val currentUser = it.data?.currentUser
                    withContext(Dispatchers.Main) {
                        emailTextView.text = currentUser?.email
                        if (currentUser?.emailVerified != true) {
                            unverifiedTextView.visibility = View.VISIBLE
                        }
                    }
                }
            } catch (e: ApolloException) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        this@SettingsActivity,
                        R.string.failed_to_execute_request,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            withContext(Dispatchers.Main) {
                container.showContent(true)
            }
        }
    }
}
