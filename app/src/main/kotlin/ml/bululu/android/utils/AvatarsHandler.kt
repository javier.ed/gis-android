package ml.bululu.android.utils

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.MenuItem
import android.widget.ImageView
import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target


class AvatarsHandler(name: String, imageView: ImageView) {
    init {
        Picasso.get().load(imageUrl(name)).transform(transformation).into(imageView)
    }

    companion object {
        private fun imageUrl(name: String) = "https://robohash.org/$name?set=set4&bgset=bg${(1..2).random()}"

        private val transformation by lazy {
            RoundedTransformationBuilder().borderColor(Color.GRAY).borderWidthDp(4f).oval(true).build()
        }

        fun loadIntoMenuItem(name: String, menuItem: MenuItem, resources: Resources) {
            val target = object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}

                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                    e?.printStackTrace()
                }

                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    val drawable: Drawable = BitmapDrawable(resources, bitmap)
                    menuItem.icon = drawable
                }
            }

            Picasso.get().load(imageUrl(name)).transform(transformation).into(target)
        }
    }
}
