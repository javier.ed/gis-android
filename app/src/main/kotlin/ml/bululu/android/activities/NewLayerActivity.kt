package ml.bululu.android.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.apollographql.apollo.exception.ApolloException
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_new_layer.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ml.bululu.android.R
import ml.bululu.android.adapters.LayerFieldsAdapter
import ml.bululu.android.models.LayerField
import ml.bululu.android.utils.FormUtils
import ml.bululu.android.utils.GraphQL
import ml.bululu.android.utils.Session

class NewLayerActivity : AppCompatActivity() {
    private val requiredFields by lazy {
        arrayOf(
            Pair<Any, TextInputLayout>(nameEditText, nameTextInputLayout),
            Pair<Any, TextInputLayout>(descriptionEditText, descriptionTextInputLayout)
        )
    }
    private val fields = mutableListOf(LayerField())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_layer)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        FormUtils.setRequiredFields(this, requiredFields)

        fieldsRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = LayerFieldsAdapter(fields)
        }

        addFieldButton.setOnClickListener {
            fields.add(LayerField())
            fieldsRecyclerView.adapter?.notifyItemInserted(fields.size - 1)

            newLayerNestedScrollView.post { newLayerNestedScrollView.fullScroll(View.FOCUS_DOWN) }
        }

        createLayerButton.setOnClickListener {
            attemptToCreateLayer()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onStart() {
        super.onStart()
        Session(this).requireAuthentication()
    }

    private fun attemptToCreateLayer() {
        FormUtils.hideKeyboard(this)
        container.showProgressBar(true)

        var fieldsAreValid = true

        fields.forEach {
            it.errorType = FormUtils.errorMessageForRequiredString(this, it.type)
            it.errorName = FormUtils.errorMessageForRequiredString(this, it.name)
            if (it.errorType != null || it.errorName != null) {
                fieldsAreValid = false
            }
        }

        if (!FormUtils.validateRequiredFields(this, requiredFields) || !fieldsAreValid) {
            fieldsRecyclerView.adapter?.notifyDataSetChanged()
            container.showContent(true)
            return
        }

        CoroutineScope(Dispatchers.IO).launch {
            val createLayerMutation =
                GraphQL(this@NewLayerActivity).mutations.createLayerMutationAsync(
                    nameEditText.text.toString(),
                    descriptionEditText.text.toString(),
                    fields
                )

            try {
                val createLayer = createLayerMutation.await().data?.createLayer
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        this@NewLayerActivity,
                        createLayer?.message,
                        Toast.LENGTH_LONG
                    ).show()
                }
                if (createLayer?.success == true) {
                    val intent = Intent(Intent(this@NewLayerActivity, MainActivity::class.java))
                    startActivity(intent)
                    finish()
                } else {
                    withContext(Dispatchers.Main) {
                        createLayer?.errors?.forEach {
                            when (it.path?.get(1)) {
                                "name" -> nameTextInputLayout.error = it.message
                                "description" -> descriptionTextInputLayout.error = it.message
                                "fields" -> {
                                    if (it.path.size == 4 && it.path[2].toIntOrNull() != null) {
                                        when (it.path[3]) {
                                            "fieldType" -> fields[it.path[2].toInt()].errorType =
                                                it.message
                                            "name" -> fields[it.path[2].toInt()].errorName =
                                                it.message
                                            "options" -> fields[it.path[2].toInt()].errorOptions =
                                                it.message
                                        }
                                    }
                                }
                            }
                        }
                        fieldsRecyclerView.adapter?.notifyDataSetChanged()
                    }
                }
            } catch (e: ApolloException) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        this@NewLayerActivity,
                        R.string.failed_to_execute_request,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            withContext(Dispatchers.Main) {
                container.showContent(true)
            }
        }
    }
}
