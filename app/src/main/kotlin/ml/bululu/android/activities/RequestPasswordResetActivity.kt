package ml.bululu.android.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.apollographql.apollo.exception.ApolloException
import kotlinx.android.synthetic.main.activity_request_password_reset.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ml.bululu.android.R
import ml.bululu.android.utils.FormUtils
import ml.bululu.android.utils.GraphQL
import ml.bululu.android.utils.Session

class RequestPasswordResetActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_password_reset)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        goToLogin.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

        FormUtils.setRequiredField(this, loginEditText, loginTextInputLayout)

        requestPasswordResetButton.setOnClickListener {
            attemptToRequestPasswordReset()
        }
    }

    override fun onStart() {
        super.onStart()
        Session(this).requireNoAuthentication()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun attemptToRequestPasswordReset() {
        FormUtils.hideKeyboard(this)
        container.showProgressBar(true)

        if (!FormUtils.validateRequiredField(this, loginEditText, loginTextInputLayout)) {
            container.showContent(true)
            return
        }

        CoroutineScope(Dispatchers.IO).launch {
            val requestPasswordResetMutation =
                GraphQL(this@RequestPasswordResetActivity).mutations.requestPasswordResetMutationAsync(
                    loginEditText.text.toString()
                )

            try {
                val requestPasswordReset =
                    requestPasswordResetMutation.await().data?.requestPasswordReset
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        this@RequestPasswordResetActivity,
                        requestPasswordReset?.message,
                        Toast.LENGTH_LONG
                    ).show()
                }
                if (requestPasswordReset?.success == true) {
                    val intent = Intent(this@RequestPasswordResetActivity, ResetPasswordActivity::class.java)
                    intent.putExtra(ResetPasswordActivity.LOGIN, loginEditText.text.toString())
                    startActivity(intent)
                    finish()
                }
            } catch (e: ApolloException) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        this@RequestPasswordResetActivity,
                        R.string.failed_to_execute_request,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            withContext(Dispatchers.Main) {
                container.showContent(true)
            }
        }
    }
}
