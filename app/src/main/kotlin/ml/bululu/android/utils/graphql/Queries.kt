package ml.bululu.android.utils.graphql

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.api.toInput
import com.apollographql.apollo.coroutines.toFlow
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import ml.bululu.android.graphql.queries.CurrentUserQuery
import ml.bululu.android.graphql.queries.LayerQuery
import ml.bululu.android.graphql.queries.LayersQuery

class Queries(private val apolloClient: ApolloClient) {
    @ExperimentalCoroutinesApi
    fun currentUserQueryFlow(): Flow<Response<CurrentUserQuery.Data>> {
        val currentUserQuery = CurrentUserQuery()
        return apolloClient.query(currentUserQuery).toFlow()
    }

    @ExperimentalCoroutinesApi
    fun layersQueryFlow(query: String? = null, after: String? = null, first: Int? = null): Flow<Response<LayersQuery.Data>> {
        val layersQuery = LayersQuery(query.toInput(), after.toInput(), first.toInput())
        return apolloClient.query(layersQuery).toFlow()
    }

    @ExperimentalCoroutinesApi
    fun layerQueryFlow(id: String): Flow<Response<LayerQuery.Data>> {
        val layerQuery = LayerQuery(id)
        return apolloClient.query(layerQuery).toFlow()
    }
}