package ml.bululu.android

import android.app.Application
import android.content.Context
import org.osmdroid.config.Configuration


class BululuApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Configuration.getInstance().load(
            applicationContext,
            applicationContext.getSharedPreferences("osmdroid_config", Context.MODE_PRIVATE)
        )
    }
}