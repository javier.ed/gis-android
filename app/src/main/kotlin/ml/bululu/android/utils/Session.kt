package ml.bululu.android.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import ml.bululu.android.activities.LoginActivity
import ml.bululu.android.activities.MainActivity
import java.util.*

class Session(private val context: Context) {
    private val prefs: SharedPreferences =
        context.getSharedPreferences("current_session", Context.MODE_PRIVATE)
    private var edit: SharedPreferences.Editor = prefs.edit()

    var id: Int = 0
        get() = prefs.getInt("id", 0)
        set(value) {
            edit = edit.putInt("id", value)
            field = value
        }

    private var key: String? = null
        get() = prefs.getString("key", null)
        set(value) {
            edit = edit.putString("key", value)
            field = value
        }

    var userId: Int = 0
        get() = prefs.getInt("userId", 0)
        set(value) {
            edit = edit.putInt("userId", value)
            field = value
        }

    var username: String? = null
        get() = prefs.getString("username", null)
        set(value) {
            edit = edit.putString("username", value)
            field = value
        }

    fun token(payload: MutableMap<String, Any>): String? {
        if (key.isNullOrBlank()) {
            return null
        }

        val key = Keys.hmacShaKeyFor(key.toString().toByteArray())
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.HOUR, -12)
        val notBefore = calendar.time
        calendar.add(Calendar.HOUR, 24)
        val expiration = calendar.time

        return Jwts.builder()
            .addClaims(payload)
            .setNotBefore(notBefore)
            .setExpiration(expiration)
            .signWith(key).compact()
    }

    fun update(
        id: Int = this.id,
        key: String? = this.key,
        userId: Int = this.userId,
        username: String? = this.username
    ): Boolean {
        this.id = id
        this.key = key
        this.userId = userId
        this.username = username
        return edit.commit()
    }

    fun clear() = edit.clear().commit()

    fun exists() = id > 0 && key?.isNotBlank() == true

    fun requireAuthentication() {
        if (!exists()) {
            context.startActivity(Intent(context, LoginActivity::class.java))
            (context as Activity?)?.finish()
        }
    }

    fun requireNoAuthentication() {
        if (exists()) {
            context.startActivity(Intent(context, MainActivity::class.java))
            (context as Activity?)?.finish()
        }
    }
}