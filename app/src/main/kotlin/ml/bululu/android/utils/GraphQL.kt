package ml.bululu.android.utils

import android.content.Context
import android.os.Build
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.CustomTypeAdapter
import com.apollographql.apollo.api.CustomTypeValue
import com.apollographql.apollo.api.Operation
import com.apollographql.apollo.api.ResponseField
import com.apollographql.apollo.cache.normalized.CacheKey
import com.apollographql.apollo.cache.normalized.CacheKeyResolver
import com.apollographql.apollo.cache.normalized.lru.EvictionPolicy
import com.apollographql.apollo.cache.normalized.lru.LruNormalizedCacheFactory
import com.apollographql.apollo.cache.normalized.sql.SqlNormalizedCacheFactory
import com.apollographql.apollo.fetcher.ApolloResponseFetchers
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import ml.bululu.android.BuildConfig
import ml.bululu.android.utils.graphql.Mutations
import ml.bululu.android.utils.graphql.Queries
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import java.net.URI
import java.util.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier

class GraphQL(private val context: Context) {
    val mutations by lazy { Mutations(apolloClient) }
    val queries by lazy { Queries(apolloClientWithCache) }

    private val apolloClient by lazy {
        ApolloClient.builder()
            .serverUrl(BuildConfig.SERVER_URL)
            .okHttpClient(getOkHttpClient())
            // .addCustomTypeAdapter(CustomType.ISO8601DATETIME, dateCustomAdapter)
            .build()
    }

    val apolloClientWithCache: ApolloClient by lazy {
        val sqlNormalizedCacheFactory = SqlNormalizedCacheFactory(context, "apollo_cache")
        val cacheKeyResolver = object : CacheKeyResolver() {
            override fun fromFieldRecordSet(
                field: ResponseField,
                recordSet: Map<String, Any>
            ): CacheKey {
                return formatCacheKey(recordSet["id"] as String?)
            }

            override fun fromFieldArguments(
                field: ResponseField,
                variables: Operation.Variables
            ): CacheKey {
                return formatCacheKey(field.resolveArgument("id", variables) as String?)
            }

            private fun formatCacheKey(id: String?): CacheKey {
                return if (id?.isNotEmpty() == true) {
                    CacheKey.from(id)
                } else {
                    CacheKey.NO_KEY
                }
            }
        }
        val memoryFirstThenSqlCacheFactory = LruNormalizedCacheFactory(
            EvictionPolicy.builder().maxSizeBytes(10 * 1024).build()
        ).chain(sqlNormalizedCacheFactory)

        ApolloClient.builder()
            .serverUrl(BuildConfig.SERVER_URL)
            .okHttpClient(getOkHttpClient())
            .normalizedCache(memoryFirstThenSqlCacheFactory, cacheKeyResolver)
            // .addCustomTypeAdapter(CustomType.ISO8601DATETIME, dateCustomAdapter)
            .defaultResponseFetcher(ApolloResponseFetchers.CACHE_AND_NETWORK)
            .build()
    }

    private val dateCustomAdapter = object : CustomTypeAdapter<Date> {
        override fun decode(value: CustomTypeValue<*>): Date {
            return DateHandler.parse(value.value.toString()) as Date
        }

        override fun encode(value: Date): CustomTypeValue<*> {
            return CustomTypeValue.GraphQLString(DateHandler.stringify(value).toString())
        }
    }

    private fun clientAppToken(payload: MutableMap<String, Any>): String? {
        val key = Keys.hmacShaKeyFor(BuildConfig.APP_KEY.toByteArray())
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.HOUR, -12)
        val notBefore = calendar.time
        calendar.add(Calendar.HOUR, 24)
        val expiration = calendar.time

        return Jwts.builder()
            .addClaims(payload)
            .setNotBefore(notBefore)
            .setExpiration(expiration)
            .signWith(key).compact()
    }

    private fun getLanguage(): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.resources.configuration.locales.toLanguageTags()
        } else {
            context.resources.configuration.locale.toString().replace("_", "-")
        }
    }

    private fun getRequest(chain: Interceptor.Chain) : Request {
        val chainRequestBuilder = chain.request().newBuilder()
        val session = Session(context)

        if (session.exists()) {
            chainRequestBuilder.addHeader("X-Session-Id", session.id.toString())
                .addHeader(
                    "X-Session-Token",
                    session.token(
                        mutableMapOf(
                            "clientAppId" to BuildConfig.APP_ID,
                            "userId" to session.userId
                        )
                    ).toString()
                )
        } else {
            chainRequestBuilder.addHeader("X-Client-App-Id", BuildConfig.APP_ID)
                .addHeader("X-Client-App-Token", clientAppToken(mutableMapOf()).toString())
        }

        return chainRequestBuilder.addHeader("Accept-Language", getLanguage()).build()
    }


    private fun getOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .hostnameVerifier(HostnameVerifier { hostname, _ -> hostname == URI(BuildConfig.SERVER_URL).host })
            .connectTimeout(1, TimeUnit.HOURS)
            .readTimeout(1, TimeUnit.HOURS)
            .writeTimeout(1, TimeUnit.HOURS)
            .addNetworkInterceptor { chain -> chain.proceed(getRequest(chain)) }
            .build()
    }
}