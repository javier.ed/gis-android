package ml.bululu.android.activities

import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.hardware.GeomagneticField
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.*
import com.apollographql.apollo.exception.ApolloException
import com.google.android.material.chip.Chip
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import ml.bululu.android.R
import ml.bululu.android.fragments.LayerInfoDialogFragment
import ml.bululu.android.fragments.UserDialogFragment
import ml.bululu.android.utils.AvatarsHandler
import ml.bululu.android.utils.GraphQL
import ml.bululu.android.utils.PermissionsHandler
import ml.bululu.android.utils.Session
import org.osmdroid.api.IMapController
import org.osmdroid.events.DelayedMapListener
import org.osmdroid.events.MapListener
import org.osmdroid.events.ScrollEvent
import org.osmdroid.events.ZoomEvent
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.overlay.compass.CompassOverlay
import org.osmdroid.views.overlay.compass.IOrientationConsumer
import org.osmdroid.views.overlay.compass.IOrientationProvider
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay

class MainActivity : AppCompatActivity(), LocationListener, IOrientationConsumer {
    private lateinit var mapController: IMapController
    private lateinit var compassOverlay: CompassOverlay
    private lateinit var myLocationOverlay: MyLocationNewOverlay
    private lateinit var rotationGestureOverlay: RotationGestureOverlay
    private lateinit var orientationProvider: IOrientationProvider
    private var initialLocationLoaded = false
    private val locationManager: LocationManager by lazy {
        getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }
    private val defaultZoom = 18.0
    private val minGpsSpeed = 10
    private var deviceOrientation = 0
    private var gpsSpeed = 0f
    private var gpsBearing = 0f
    private var latitude = 0f
    private var longitude = 0f
    private var altitude = 0f
    private var timeOfFix: Long = 0
    private var latitudeBeforeMoving = 0.0
    private var longitudeBeforeMoving = 0.0
    private var currentArea = 2000f
    private val session by lazy { Session(this) }
    private lateinit var layersJob: Job
    private val layerChips = mutableListOf<Pair<String, Chip>>()
    private var currentLayerId: String? = null

    companion object {
        const val LAYER_ID = "LAYER_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val extras = intent.extras
        currentLayerId = extras?.getString(LAYER_ID)

        setupInsets()
        loadMap()
        loadLocationFAB()

        layerInfoChip.setOnClickListener {
            currentLayerId?.let { id -> LayerInfoDialogFragment.newInstance(id).show(supportFragmentManager, "layer_${id}") }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.user, menu)
        if (session.exists()) {
            val userItem = menu!!.findItem(R.id.userItem)
            userItem.setIcon(R.drawable.ic_account_circle_32dp)
            userItem.title = session.username
            AvatarsHandler.loadIntoMenuItem(
                session.username.toString(),
                userItem as MenuItem,
                resources
            )
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (session.exists()) {
            UserDialogFragment.newInstance().show(supportFragmentManager, session.username)
        } else {
            if (item.itemId == R.id.userItem) {
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    @ExperimentalCoroutinesApi
    override fun onStart() {
        super.onStart()
        PermissionsHandler(this).request {
            loadLocationManager()
            loadLayers()
            map.onResume()
        }
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
        try {
            locationManager.removeUpdates(this)
        } catch (e: SecurityException) {
            e.printStackTrace()
        }
        if (::orientationProvider.isInitialized) {
            orientationProvider.stopOrientationProvider()
        }
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
    }

    override fun onStop() {
        super.onStop()
        layerInfoChip.isVisible = false
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::myLocationOverlay.isInitialized) {
            myLocationOverlay.disableMyLocation()
            myLocationOverlay.disableFollowLocation()
        }
    }

    private fun setupInsets() {
        container.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION

        ViewCompat.setOnApplyWindowInsetsListener(toolbar) { _, insets ->
            toolbar.setMarginTop(insets.systemWindowInsetTop)

            fabsLinealLayout.setMarginTop(fabsLinealLayout.marginTop + insets.systemWindowInsetTop)
            layerInfoChip.setMarginTop(layerInfoChip.marginTop + insets.systemWindowInsetTop)

            val layoutParams = layersHorizontalScrollView.layoutParams as FrameLayout.LayoutParams
            layoutParams.bottomMargin = layersHorizontalScrollView.marginBottom + insets.systemWindowInsetBottom
            layersHorizontalScrollView.layoutParams = layoutParams

            insets
        }
    }

    private fun View.setMarginTop(value: Int) = updateLayoutParams<ViewGroup.MarginLayoutParams> {
        topMargin = value
    }


    private fun loadLocationManager() {
        if (!"Android-x86".equals(Build.BRAND, ignoreCase = true)) {
            //lock the device in current screen orientation
            requestedOrientation =
                when ((getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.rotation) {
                    Surface.ROTATION_0 -> {
                        deviceOrientation = 0
                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                    }
                    Surface.ROTATION_90 -> {
                        deviceOrientation = 90
                        ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                    }
                    Surface.ROTATION_180 -> {
                        deviceOrientation = 180
                        ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT
                    }
                    else -> {
                        deviceOrientation = 270
                        ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
                    }
                }
        }

        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this)
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this)
        } catch (e: SecurityException) {
            e.printStackTrace()
        }

        orientationProvider = InternalCompassOrientationProvider(this)
        orientationProvider.startOrientationProvider(this)
    }

    private fun calculateDistance(
        startLatitude: Double,
        startLongitude: Double,
        endLatitude: Double,
        endLongitude: Double
    ): Float {
        val distance = floatArrayOf(1f)
        Location.distanceBetween(
            startLatitude,
            startLongitude,
            endLatitude,
            endLongitude,
            distance
        )
        return distance[0]
    }

    private fun setCurrentArea() {
        currentArea = calculateDistance(0.0, 0.0, map.latitudeSpanDouble, map.longitudeSpanDouble)
    }

    private fun loadMap() {
        map.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE)

        map.zoomController.setVisibility(CustomZoomButtonsController.Visibility.NEVER)
        map.setMultiTouchControls(true)

        mapController = map.controller
        mapController.setZoom(defaultZoom)

        compassOverlay = CompassOverlay(this, map)
        compassOverlay.setCompassCenter(40f, 128f)
        map.overlays.add(compassOverlay)

        rotationGestureOverlay = RotationGestureOverlay(map)
        rotationGestureOverlay.isEnabled = true
        map.overlays.add(rotationGestureOverlay)

        val pointerBitmap = getDrawable(R.drawable.ic_pointer)?.toBitmap()
        myLocationOverlay = MyLocationNewOverlay(GpsMyLocationProvider(this@MainActivity), map)
        myLocationOverlay.enableMyLocation()
        myLocationOverlay.enableAutoStop = false
        myLocationOverlay.setPersonIcon(pointerBitmap)
        myLocationOverlay.setPersonHotspot(30f, 30f)
        myLocationOverlay.setDirectionArrow(pointerBitmap, pointerBitmap)
        map.overlays.add(myLocationOverlay)

        map.addMapListener(DelayedMapListener(object : MapListener {
            override fun onScroll(event: ScrollEvent?): Boolean {
                setCurrentArea()
                return true
            }

            override fun onZoom(event: ZoomEvent?): Boolean {
                setCurrentArea()
                return true
            }
        }, 250))
    }

    private fun loadLocationFAB() {
        locationFAB.setOnClickListener {
            if (myLocationOverlay.isFollowLocationEnabled) {
                myLocationOverlay.disableFollowLocation()
                rotationGestureOverlay.isEnabled = true
                locationFAB.setImageResource(R.drawable.ic_location_searching_24dp)
                compassOverlay.disableCompass()
            } else {
                myLocationOverlay.enableFollowLocation()
                rotationGestureOverlay.isEnabled = false
                mapController.setZoom(defaultZoom)
                locationFAB.setImageResource(R.drawable.ic_my_location_24dp)
                compassOverlay.enableCompass()
                setCurrentArea()
            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun loadLayers() {
        layersJob = CoroutineScope(Dispatchers.IO).launch {
            val layersQuery = GraphQL(this@MainActivity).queries.layersQueryFlow()

            try {
                layersQuery.collect {
                    withContext(Dispatchers.Main) {
                        layerChips.clear()
                        layersChipGroup.removeAllViews()
                        it.data?.layers?.nodes?.forEach { node ->
                            val chip = Chip(this@MainActivity)
                            chip.text = node?.name
                            chip.isCheckable = true
                            chip.setTextAppearance(R.style.AppTheme_TextAppearance_Chip)
                            chip.elevation = 1f
                            chip.setOnCheckedChangeListener { _, isChecked ->
                                layerInfoChip.isVisible = when {
                                    isChecked -> {
                                        currentLayerId = node?.id
                                        layerInfoChip.text = node?.name
                                        layerChips.filter { c -> c.first != node?.id && c.second.isChecked }.forEach { c -> c.second.isChecked = false }
                                        true
                                    }
                                    layerChips.find { c -> c.second.isChecked } != null -> true
                                    else -> {
                                        currentLayerId = null
                                        false
                                    }
                                }
                            }

                            if (currentLayerId != null && node?.id == currentLayerId) {
                                chip.isChecked = true
                            }

                            layersChipGroup.addView(chip)
                            layerChips.add(Pair(node?.id.toString(), chip))
                        }
                    }
                }
            } catch (e: ApolloException) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        this@MainActivity,
                        R.string.failed_to_execute_request,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            withContext(Dispatchers.Main) {
                val chip = Chip(this@MainActivity)
                chip.setText(R.string.view_more)
                chip.setChipIconResource(R.drawable.ic_layers_24dp)
                chip.setTextAppearance(R.style.AppTheme_TextAppearance_Chip)
                chip.elevation = 1f
                chip.setOnClickListener {
                    startActivity(Intent(this@MainActivity, LayersActivity::class.java))
                }
                layersChipGroup.addView(chip)
            }
        }
    }

    private fun setMapOrientation(bearing: Float) {
        var t = 360 - bearing - deviceOrientation
        if (t < 0) {
            t += 360f
        }
        if (t > 360) {
            t -= 360f
        }
//        t = t.toInt().toFloat()
//        t /= 5
//        t = t.toInt().toFloat()
//        t *= 5
        map.mapOrientation = t
    }

    override fun onLocationChanged(location: Location) {
        if (!initialLocationLoaded) {
            val startPoint = GeoPoint(location.latitude, location.longitude)
            mapController.setCenter(startPoint)
            latitudeBeforeMoving = latitude.toDouble()
            longitudeBeforeMoving = longitude.toDouble()
            initialLocationLoaded = true
        }

        gpsBearing = location.bearing
        gpsSpeed = location.speed
        latitude = location.latitude.toFloat()
        longitude = location.longitude.toFloat()
        altitude = location.altitude.toFloat()
        timeOfFix = location.time

        if (myLocationOverlay.isFollowLocationEnabled) {
            if (gpsSpeed >= minGpsSpeed) {
                //use gps bearing instead of the compass
                setMapOrientation(gpsBearing)
            }
        }
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {}

    override fun onProviderEnabled(p0: String?) {}

    override fun onProviderDisabled(p0: String?) {}

    override fun onOrientationChanged(orientation: Float, source: IOrientationProvider?) {
        //only use the compass bit if we aren't moving, since gps is more accurate when we are moving
        if (myLocationOverlay.isFollowLocationEnabled && gpsSpeed < minGpsSpeed) {
            val geomagneticField = GeomagneticField(latitude, longitude, altitude, timeOfFix)
            var trueNorth = orientation + geomagneticField.declination
            synchronized(trueNorth) {
                if (trueNorth > 360.0f) {
                    trueNorth -= 360.0f
                }
                //this part adjusts the desired map rotation based on device orientation and compass heading
                setMapOrientation(trueNorth)
            }
        }
    }
}
