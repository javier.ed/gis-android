package ml.bululu.android.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.apollographql.apollo.exception.ApolloException
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ml.bululu.android.R
import ml.bululu.android.utils.FormUtils
import ml.bululu.android.utils.GraphQL
import ml.bululu.android.utils.Session

class LoginActivity : AppCompatActivity() {
    private val requiredFields by lazy {
        arrayOf(
            Pair<Any, TextInputLayout>(loginEditText, loginTextInputLayout),
            Pair<Any, TextInputLayout>(passwordEditText, passwordTextInputLayout)
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        goToRequestPasswordResetButton.setOnClickListener {
            startActivity(Intent(this, RequestPasswordResetActivity::class.java))
        }

        goToRegisterButton.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }

        FormUtils.setRequiredFields(this, requiredFields)

        loginButton.setOnClickListener {
            attemptToLogin()
        }
    }

    override fun onStart() {
        super.onStart()
        Session(this).requireNoAuthentication()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun attemptToLogin() {
        FormUtils.hideKeyboard(this)
        container.showProgressBar(true)

        if (!FormUtils.validateRequiredFields(this, requiredFields)) {
            container.showContent(true)
            return
        }

        CoroutineScope(Dispatchers.IO).launch {
            val loginMutation = GraphQL(this@LoginActivity).mutations.loginMutationAsync(
                loginEditText.text.toString(),
                passwordEditText.text.toString()
            )

            try {
                val login = loginMutation.await().data?.login

                withContext(Dispatchers.Main) {
                    Toast.makeText(this@LoginActivity, login?.message, Toast.LENGTH_LONG).show()
                }
                if (login?.success == true) {
                    Session(this@LoginActivity).update(
                        login.session?.id?.toInt() as Int,
                        login.session.key,
                        login.user?.id?.toInt() as Int,
                        login.user.username
                    )
                    val intent = Intent(Intent(this@LoginActivity, MainActivity::class.java))
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                    finish()
                }
            } catch (e: ApolloException) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        this@LoginActivity,
                        R.string.failed_to_execute_request,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            withContext(Dispatchers.Main) {
                container.showContent(true)
            }
        }
    }
}
